// 1. What directive is used by Node.js in loading the modules it needs?

// Import directive

// 2. What Node.js module contains a method for server creation?

// http

// 3. What is the method of the http object responsible for creating a server using Node.js?
 
/*
1. const server = http. createServer((req, res) => {
2. res. end('Server is running!' );
3. server. listen(8080, () => {
4. const { address, port } = server. address();
5. console. log(`Server is listening on: http://${address}:${port}`);
*/

// 4. What method of the response object allows us to set status codes and content types?

// writeHead method

// 5. Where will console.log() output its contents when run in Node.js?
// Terminal

// 6. What property of the request object contains the address's endpoint?
// request.url
